<?php
$d->reset();
$sql_da = "select id,ten$lang as ten,tenkhongdau,thumb,photo,mota$lang as mota,ngaytao FROM #_news where  noibat>0 and type='duan' and hienthi=1 order by stt asc limit 0,20";
$d->query($sql_da);
$mn_duan=$d->result_array();

$d->reset();
$sql_tt = "select id,ten$lang as ten,tenkhongdau,thumb,photo,mota$lang as mota,ngaytao FROM #_news where  noibat>0 and type='tintuc' and hienthi=1 order by stt asc limit 0,20";
$d->query($sql_tt);
$mn_tintuc=$d->result_array();

$d->reset();
$sql="select ten$lang as ten,tenkhongdau,id,thumb from #_news_danhmuc where type='dichvu' and hienthi=1 order by stt asc,id desc";
$d->query($sql);
$mn_dichvu=$d->result_array();

$d->reset();
$sql_tt = "select  n.title$lang as title, n.photo as img,n.thumb as thumbs,d.tenkhongdau as tkd ,n.tenkhongdau as tenkhongdau , n.mota as mota , n.ngaytao as ngaytao, d.ten$lang as tendanhmuc
           FROM table_news as n inner join table_news_danhmuc as d on n.id_danhmuc = d.id where n.type='dichvu' and d.hienthi=1 and d.noibat = 1 order by d.stt asc, d.id desc";
$d->query($sql_tt);
$dichvu_nd = $d->result_array();

$d->reset();
$sql_dm = "select  DISTINCT d.ten$lang as tendanhmuc, d.tenkhongdau as tkd FROM table_news as n inner join table_news_danhmuc as d on n.id_danhmuc = d.id where n.type='dichvu' and d.hienthi=1 and d.noibat = 1";
$d->query($sql_dm);
$dichvu_danhmuc = $d->result_array();

?>
<?php if (count($tintuc) >=0 ) { ?>
<?php if ($id_danhmuc == "" && $tintuc[0]['type']=="dichvu") { ?>
<section class="container-fluid container-wrap home-product wow bounceInUp" id="home-product">
    <div class="tieude_dichvu wow bounceInUp">
        DỊCH VỤ NỔI BẬT
        <p> Công ty của chúng tôi tự hào là nhà cung cấp các dịch vụ tiểu cảnh sân vườn cùng với những sản phẩm liên quan tốt nhất .</p>
    </div>
    <div class="menu">
        <ul>
            <?php foreach($dichvu_danhmuc as $k => $danhmuc) {
                if($danhmuc['tendanhmuc'] != "") {
                    if ($k > 3) {
                        break;
                    }else {?>
                        <li rel="<?php echo $k ?>" class="col-md-2.5 col-sm-2.5 col-xs-6 <?php if ($k==0) echo "active"; ?>"><?php echo $danhmuc['tendanhmuc'];?></li>
                    <?php }}}?>
        </ul>
    </div>
    <?php foreach($dichvu_danhmuc as $k => $danhmuc) { if($danhmuc['tendanhmuc'] != "") {
        if($danhmuc['tendanhmuc'] != "") {
            if ($k > 3) {
                break;
            }else {?>
                <div class="home-product-content home-product-content<?php echo $k ?>" <?php if ($k==0) echo 'style="display:block"' ?>>
                    <div class="row">
                        <?php foreach($dichvu_nd as $k => $value) { ?>
                            <?php if($value['tkd'] == $danhmuc['tkd']){ ?>
                                <div class="col-6 col-md-3 text-center">
                                    <div class="img_dichvu">
                                        <div class="image">
                                            <a href=/dich-vu/<?=$value['tenkhongdau']?>.html title="<?=$value['title']?>" class="overlay-img">
                                                <img class="img_product_dichvu" src="thumb/350x300/1/<?=_upload_tintuc_l.$value['thumbs']?>" alt="<?php echo $value['title'] ?>">
                                            </a>
                                        </div>
                                        <a href=/dich-vu/<?=$value['tenkhongdau']?>.html title="<?=$value['title']?>">
                                            <h3 class="title">
                                                <?php echo $value['title'] ?>
                                            </h3>
                                        </a>
                                        <div class="address">
                                            <?= catchuoi($value['mota'], 60) ?>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            <?php }?>
                        <?php } ?>
                    </div>
                </div>
            <?php }}}} ?>
</section>


<?php } else { ?>


<h1 class="tieude_giua"><span><?=$title_cat?></span></h1>
<section class="main-content container-fluid container-wrap">
    <div class="col-md-9 main-content-detail">
        <div class="post-detail">
            <div class="change-view hidden-xs hidden-sm">
                <span title="Xem dạng danh sách" id="list-view"> <i class="fa fa-list"></i></span>
                <span title="Xem dạng lưới" id="grid-view"><i class="fa fa-th-large"></i></span></div>
            Cập nhật các bài viết mới nhất về chia sẻ kinh nghiệm , hướng dẫn trong ngành cảnh quan , cá koi ,
            cây xanh , phong thủy cảnh quan, tin tức ngành cảnh quan (landscape)
            <div class="post-list-article post-list-article-list">
                <?php for($i = 0, $count_tintuc = count($tintuc); $i < $count_tintuc; $i++){ ?>
                <div class="col-md-12 item-article">
                    <a class="col-md-3 padding8px" href="<?=$com?>/<?=$tintuc[$i]['tenkhongdau']?>.html" title="<?=$tintuc[$i]['ten']?>">
                        <img src="<?php if($tintuc[$i]['thumb']!=NULL)echo _upload_tintuc_l.$tintuc[$i]['thumb'];else echo 'images/noimage.png';?>" alt="<?=$tintuc[$i]['ten']?>"/></a>
                    <h4 class="title">
                        <a href="<?=$com?>/<?=$tintuc[$i]['tenkhongdau']?>.html" title="<?=$tintuc[$i]['ten']?>"><?=$tintuc[$i]['ten']?></a>
                    </h4>
                    <p class="desc"><?=catchuoi($tintuc[$i]['mota'],220)?></p>
                </div>
                <?php } ?>
                <div class="clear"></div>
                <div class="pagination"><?=pagesListLimitadmin($url_link , $totalRows , $pageSize, $offset)?></div>
            </div>
        </div>
        <div class="col-xs-12 padding-0" style="clear:both" id="rating" rel="35.231.153.251" path="kinh-nghiem-tin-tuc.html">
            <p id="rating_count" count="4">Đánh giá : <span style="color: #999;font-size:14px">4 (80%) 9 votes</span></p>
        </div>
        <div class="sharer" style="clear:both" sharer-enable="1">
            <p class="sharer-title">Chia sẻ :</p>
            <div class="addthis_inline_share_toolbox"></div>
        </div>
        <div class="post-comment" fb-enable="1" fb-rel="2391526854491822">
            <div class="fb-comments" data-href="https://sanvuontrucxinh.com/kinh-nghiem-tin-tuc"
                 data-numposts="20" data-width="100%"></div>
        </div>
        <div class="post-relation">
            <p class="post-relation-title">Bài viết - Chuyên mục liên quan</p>
            <?php if(count($tintuc) > 0) { ?>
                <div class="othernews">
                    <p class="post-relation-title"><?=$title_other?></p>
                    <ul class="phantrang">
                        <?php for($i = 0, $count_tintuc = count($tintuc); $i < $count_tintuc; $i++){ ?>
                            <li><a href="<?=$com?>/<?=$tintuc[$i]['tenkhongdau']?>.html" title="<?=$tintuc[$i]['ten']?>"><?=$tintuc[$i]['ten']?></a> (<?=make_date($tintuc[$i]['ngaytao'])?>)</li>
                        <?php }?>
                    </ul>
                    <div class="pagination"><?=pagesListLimitadmin($url_link , $totalRows , $pageSize, $offset)?></div>
                </div><!--.othernews-->
            <?php }?>
        </div>
    </div>
    <div class="col-md-3 sidebar">
        <div class="sidebar-search">
            <form>
                <input type="text" id="keyword" placeholder="Tìm kiếm nhanh" name="keyword" value="<?=@$tukhoa?>" required />
                <button class="btn btn-success btn-flat" type="submit"><i class="fa fa-search"></i>
                    <p class=" btn_search" aria-hidden="true" title="<?=_search?>" onclick="onSearch(event,'keyword');" ></p></button>
            </form>
        </div>
        <div class="sidebar-category" data-spy="affix" data-offset-top="160" data-offset-bottom="1500">
            <p class="sidebar-category-title">Danh mục chính</p>
            <ul>
                <li>
                    <a class="menu"
                       role="button" data-toggle="collapse" data-parent="#accordion"
                       href="#collapse255" aria-expanded="true" aria-controls="collapse255">
                       <i class="fa fa-leaf"></i> <?=_dichvu?> <i class="fa fa-chevron-down right"></i>
                    </a>
                    <?php if(count($mn_dichvu)>0) { ?>
                    <ul id="collapse255" class="panel-collapse collapse submenu" role="tabpanel" aria-labelledby="heading255">
                        <?php for($i = 0; $i < count($mn_dichvu); $i++){ ?>
                             <li><a href="dich-vu/<?=$mn_dichvu[$i]['tenkhongdau']?>/">+ <?=$mn_dichvu[$i]['ten']?></a></li>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                </li>
                <li>
                    <a class="menu"
                       role="button" data-toggle="collapse" data-parent="#accordion"
                       href="#collapse25" aria-expanded="true" aria-controls="collapse255">
                        <i class="fa fa-leaf"></i> <?=_tintuc?> <i class="fa fa-chevron-down right"></i>
                    </a>
                    <?php if(count($mn_tintuc)>0) { ?>
                        <?php for($i = 0; $i < count($mn_tintuc); $i++){ ?>
                            <ul id="collapse25" class="panel-collapse collapse submenu" role="tabpanel" aria-labelledby="heading255">
                                <?php foreach($mn_tintuc as $k => $value){   ?>
                                    <li><a href="tin-tuc/<?=$value['tenkhongdau']?>.html">+ <?=catchuoi($value['ten'],30)?></a></li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    <?php } ?>
                </li>
                <li>
                    <a class="menu"
                       role="button" data-toggle="collapse" data-parent="#accordion"
                       href="#collapse257" aria-expanded="true" aria-controls="collapse257">
                        <i class="fa fa-leaf"></i> <?=_duan?> <i class="fa fa-chevron-down right"></i>
                    </a>
                    <?php if(count($mn_duan)>0) { ?>
                        <?php for($i = 0; $i < count($mn_duan); $i++){ ?>
                            <ul id="collapse257" class="panel-collapse collapse submenu" role="tabpanel" aria-labelledby="heading255">
                                <?php foreach($mn_duan as $k => $value){   ?>
                                    <li><a href="du-an/<?=$value['tenkhongdau']?>.html">+ <?=catchuoi($value['ten'],30)?></a></li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    <?php } ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<?php } }?>

