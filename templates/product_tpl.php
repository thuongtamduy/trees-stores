<?php
$d->reset();
$sql_ttt = "select n.title$lang as title,n.tenkhongdau as tkd, n.thumb as thumb,n.ten$lang as ten,n.mota$lang as mota,n.gia as gia,d.ten$lang as tendanhmuc, d.tenkhongdau as tkddanhmuc  FROM table_product as n inner join table_product_danhmuc as d on n.id_danhmuc = d.id where n.type='sanpham' and n.hienthi=1";
$d->query($sql_ttt);
$product_nd = $d->result_array();

$d->reset();
$sql_ttt1 = "select DISTINCT d.tenkhongdau as tkddanhmuc, d.ten$lang as tendanhmuc  FROM table_product as n inner join table_product_danhmuc as d on n.id_danhmuc = d.id where n.type='sanpham' and n.hienthi=1";
$d->query($sql_ttt1);
$product_danhmuc = $d->result_array();

$d->reset();
$sql_tt = "select ten$lang,photo FROM table_slider where type='background_slider' and hienthi=1 order by stt asc limit 0,20";
$d->query($sql_tt);
$background_slider = $d->result_array();
?>
<input type="hidden" value="1" class="soluong"  />


<div style="text-align: justify;">
    <div id="swiper-container1" class="swiper-container">
        <div class="swiper-wrapper">
            <?php foreach ($background_slider as $k => $value) { ?>
                <div class="swiper-slide">
                    <img style="image-rendering: pixelated;" src="<?=_upload_hinhanh_l.$value['photo'] ?>" alt="<?= $value['ten'] ?>" class="swiper-slide"/>
                </div>
            <?php } ?>
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>
<br>
<h1 class="tieude_giua"><span><?=$title_cat?></span></h1>
<br>


<?php if ($id_danhmuc == "") { ?>
    <?php foreach($product_danhmuc as $k => $danhmuc){ if (!empty($danhmuc['tkddanhmuc'])) {?>
        <section class="container-fluid container-wrap home-blog home-blog1" id="home-blog">
            <div class="row">
                <div class="col-md-12 product-danhmuc" data-wow-delay="1s">
                    <h2 ><span><?php echo $danhmuc['tendanhmuc']; ?></span></h2>
                </div>
                <div class=" wow slideInUp col-md-12 home-blog-row owl-carousel">
                    <?php foreach($product_nd as $k => $value){ if ($value["tkddanhmuc"] == $danhmuc["tkddanhmuc"]) {?>
                        <div class="item-blog wow pulse">
                            <div class="featured-img">
                                <a href="san-pham/<?=$value['tkd']?>.html" title="<?=$value['ten']?>">
                                    <img src="thumb/370x250/1/<?=_upload_sanpham_l.$value['thumb']?>" alt="<?=$value['ten']?>">
                                </a>
                            </div>
                            <div class="content-blog">
                                <a href="san-pham/<?=$value['tkd']?>.html" class="title"  ><?=$value['ten']?></a>
                                <p class="height50"><?=catchuoi($value['mota'],100)?></p>
                                <p class="readmore"><a href="san-pham/<?=$value['tkd']?>.html" >Xem thêm</a></p>
                            </div>
                        </div>
                    <?php }} ?>
                </div>
            </div>
        </section>
    <?php } } } else { ?>

    <section class="container-fluid container-wrap home-blog home-blog1" id="home-blog">
        <div class="row">
            <div class="col-md-12 product-danhmuc" data-wow-delay="1s">
                <?php foreach($product_danhmuc as $k => $danhmuc) { if ($danhmuc['tkddanhmuc'] == $id_danhmuc) { ?>
                    <h2 ><span><?php echo $danhmuc['tendanhmuc']; ?></span></h2>
                <?php } }?>
            </div>
            <div class=" wow slideInUp col-md-12 home-blog-row owl-carousel">
                <?php foreach($product_nd as $k => $value){ if ($value["tkddanhmuc"] == $id_danhmuc) {?>
                    <div class="item-blog wow pulse">
                        <div class="featured-img">
                            <a href="san-pham/<?=$value['tkd']?>.html" title="<?=$value['ten']?>">
                                <img src="thumb/370x250/1/<?=_upload_sanpham_l.$value['thumb']?>" alt="<?=$value['ten']?>">
                            </a>
                        </div>
                        <div class="content-blog">
                            <a href="san-pham/<?=$value['tkd']?>.html" class="title"  ><?=$value['ten']?></a>
                            <p class="height50"><?=catchuoi($value['mota'],100)?></p>
                            <p class="readmore"><a href="san-pham/<?=$value['tkd']?>.html" >Xem thêm</a></p>
                        </div>
                    </div>
                <?php }} ?>
            </div>
        </div>
    </section>

<?php } ?>


<br>
<br><br>

<h1 class="tieude_giua"><span>SẢN PHẨM LIÊN QUAN</span></h1>

<section class="container-fluid container-wrap home-blog home-blog1" id="home-blog">
    <div class="row">
        <div class=" wow slideInUp col-md-12 home-blog-row owl-carousel">
            <?php foreach($product_nd as $k => $value){?>
                <div class="item-blog wow pulse">
                    <div class="featured-img">
                        <a href="san-pham/<?=$value['tkd']?>.html" title="<?=$value['ten']?>">
                            <img src="thumb/370x250/1/<?=_upload_sanpham_l.$value['thumb']?>" alt="<?=$value['ten']?>">
                        </a>
                    </div>
                    <div class="content-blog">
                        <a href="san-pham/<?=$value['tkd']?>.html" class="title"  ><?=$value['ten']?></a>
                        <p class="height50"><?=catchuoi($value['mota'],50)?></p>
                        <p class="readmore"><a href="san-pham/<?=$value['tkd']?>.html" >Xem thêm</a></p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<!--<script>-->
<!--    $(document).ready(function() {-->
<!---->
<!--        var owl = $("#owl-demo");-->
<!---->
<!--        owl.owlCarousel({-->
<!--            items : 5, //10 items above 1000px browser width-->
<!--            itemsDesktop : [1000,5], //5 items between 1000px and 901px-->
<!--            itemsDesktopSmall : [900,3], // betweem 900px and 601px-->
<!--            itemsTablet: [600,2], //2 items between 600 and 0-->
<!--            itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option-->
<!--        });-->
<!---->
<!--        // Custom Navigation Events-->
<!--        $(".btn .next").click(function(){-->
<!--            owl.trigger('owl.next');-->
<!--        })-->
<!--        $(".btn .prev").click(function(){-->
<!--            owl.trigger('owl.prev');-->
<!--        })-->
<!--    });-->
<!--</script>-->







