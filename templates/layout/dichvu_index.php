<?php
$d->reset();
$sql_tt = "select  n.title$lang as title, n.photo as img,n.thumb as thumbs,d.tenkhongdau as tkd ,n.tenkhongdau as tenkhongdau , n.mota as mota , n.ngaytao as ngaytao, d.ten$lang as tendanhmuc
           FROM table_news as n inner join table_news_danhmuc as d on n.id_danhmuc = d.id where n.type='dichvu' and d.hienthi=1 and d.noibat = 1";
$d->query($sql_tt);
$dichvu_nd = $d->result_array();

$d->reset();
$sql_dm = "select  DISTINCT d.ten$lang as tendanhmuc, d.tenkhongdau as tkd FROM table_news as n inner join table_news_danhmuc as d on n.id_danhmuc = d.id where n.type='dichvu' and d.hienthi=1 and d.noibat = 1";
$d->query($sql_dm);
$dichvu_danhmuc = $d->result_array();
?>
<section class="container-fluid container-wrap home-product wow bounceInUp" id="home-product">
        <div class="tieude_dichvu wow bounceInUp">
             DỊCH VỤ CỦA CHÚNG TÔI
            <p> Công ty của chúng tôi tự hào là nhà cung cấp các dịch vụ tiểu cảnh sân vườn cùng với những sản phẩm liên quan tốt nhất .</p>
        </div>
    <div class="menu">
        <ul>
            <?php foreach($dichvu_danhmuc as $k => $danhmuc) {
                if($danhmuc['tendanhmuc'] != "") {
                    if ($k > 3) {
                        break;
                    }else {?>
                        <li rel="<?php echo $k ?>" class="col-md-2 col-sm-auto col-xs-6 <?php if ($k==0) echo "active"; ?>"><?php echo $danhmuc['tendanhmuc'];?></li>
            <?php }}}?>
        </ul>
    </div>
    <?php foreach($dichvu_danhmuc as $k => $danhmuc) { if($danhmuc['tendanhmuc'] != "") {
    if($danhmuc['tendanhmuc'] != "") {
        if ($k > 3) {
            break;
        }else {?>
    <div class="home-product-content home-product-content<?php echo $k ?>" <?php if ($k==0) echo 'style="display:block"' ?>>
        <div class="row">
            <?php foreach($dichvu_nd as $k => $value) { ?>
                <?php if($value['tkd'] == $danhmuc['tkd']){ ?>
                    <div class="col-6 col-md-3 text-center">
                        <div class="img_dichvu">
                            <div class="image">
                                <a href=/dich-vu/<?=$value['tenkhongdau']?>.html title="<?=$value['title']?>" class="overlay-img">
                                    <img class="img_product_dichvu" src="thumb/350x300/1/<?=_upload_tintuc_l.$value['thumbs']?>" alt="<?php echo $value['title'] ?>">
                                </a>
                            </div>
                            <a href=/dich-vu/<?=$value['tenkhongdau']?>.html title="<?=$value['title']?>">
                                <h3 class="title">
                                    <?php echo $value['title'] ?>
                                </h3>
                            </a>
                            <div class="address">
                                <?= catchuoi($value['mota'], 60) ?>
                            </div>
                            <br>
                        </div>
                    </div>
                <?php }?>
            <?php } ?>
        </div>
    </div>
    <?php }}}} ?>
</section>