<?php


$d->reset();
$sql_tt = "select id,ten$lang as ten,tenkhongdau,thumb,photo,mota$lang as mota,ngaytao FROM #_news where type='tintuc' and noibat>0 and hienthi=1 order by stt asc limit 0,20";
$d->query($sql_tt);
$tintuc_i = $d->result_array();


@define ( '_template' , './templates/');
@define ( '_source' , './sources/');
@define ( '_lib' , './admin/lib/');

include_once _lib."Mobile_Detect.php";
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');

?>
<div id="tintuc_index">

	<div class="content_noidung">


        <div class="col_w50 wow fadeInUp" data-wow-delay="500ms">
            <div class="title_tintuc"><span>VIDEO CLIP</span></div>

            <div class="load_video"></div>

        </div>


        <div class="col_w50 wow fadeInUp" data-wow-delay="500ms">
            <div class="title_tintuc"><span>TIN TỨC & SỰ KIỆN</span></div>

            <div id="slick_tintuc_i" >
            <?php foreach($tintuc_i as $k => $value){   ?>

                <div class="box_news_index">
                    <a href="tin-tuc/<?=$value['tenkhongdau']?>.html" title="<?=$value['ten']?>"><img class="img" src="thumb/220x220/1/<?=_upload_tintuc_l.$value['photo']?>" alt="<?=$value['ten']?>" /></a>

                    <h3 class="ten">
                        <a href="tin-tuc/<?=$value['tenkhongdau']?>.html" title="<?=$value['ten']?>"><?=$value['ten']?></a>
                    </h3>
                    <div class="mota"><?=catchuoi($value['mota'],100)?></div>
       
                </div>
                  
            <?php } ?> 
            </div>
                    <?php  if($deviceType=='computer') include _template."layout/slider_tintuc.php";?>
        </div>
        <div class="clear"></div>
    </div>
</div>

<?php /* ?>

<div class="fb-page" data-href="<?=$company['fanpage']?>" data-tabs="timeline" data-width="500px"  data-height="330px" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="<?=$company['fanpage']?>" class="fb-xfbml-parse-ignore"><a href="<?=$company['fanpage']?>"><?=$company['fanpage']?></a></blockquote></div>

<?php */ ?>


