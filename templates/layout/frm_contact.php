
<div class="contact-form">
    <form action="post" method="post" name="frm" class="frm" action="lien-he.html" enctype="multipart/form-data">
        <div class="form-group">
            <div class="input-group"><span class="input-group-addon color"><span class="fa fa-user"></span></span>
                <input name="ten_lienhe" type="text" class="form-control" id="ten_lienhe" placeholder="<?=_hovaten?> (*)" required/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group"><span class="input-group-addon color"><span class="fa fa-phone"></span></span>
                <input class="form-control" name="dienthoai_lienhe" type="text" id="dienthoai_lienhe" placeholder="<?=_dienthoai?> (*)" required/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group"><span class="input-group-addon color"><span class="fa fa-envelope"></span></span>
                <input name="email_lienhe" type="text" id="email_lienhe" class="form-control"  placeholder="Email"  required/>
            </div>
        </div>
        <div class="form-group">
            <textarea class="form-control" name="noidung_lienhe" id="noidung_lienhe" class="input_lh"  rows="3" placeholder="<?=_noidung?>" required></textarea>
        </div>
        <div class="form-group">
            <button type="button" value="GỬI THÔNG TIN" class="click_ajax">
                <i class=" fa fa-send"></i> GỬI THÔNG TIN </button>
        <button type="reset" class="click_ajax"><i class="fa fa-refresh"></i> Làm lại </button>
        </div>
    </form>
    <div id="result_contact"></div>
</div>