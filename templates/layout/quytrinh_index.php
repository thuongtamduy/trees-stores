<div class="text-center content_quytrinh">
<div class="tieude_dichvu">
	QUY TRÌNH CỦA CHÚNG TÔI
</div>
	<p>Trạng Nguyên luôn đưa ra một quy trình đơn giản, đầy đủ và chi tiết nhất cho khách hàng</p>
	<div class="row content_quytrinh">
		<div class="text-center col-xs-6 col-md-2 item-process wow swing">
			<img class="img_quytrinh" src="./images/khao_sat.png" alt="">
			<p>1.Khảo sát</p>
		</div>
		<div class="text-center col-xs-6 col-md-2 item-process wow swing">
			<img class="img_quytrinh" src="./images/bao_gia.png" alt="">
			<p>2.Báo Giá</p>
		</div>
		<div class="text-center col-xs-6 col-md-2 item-process wow swing">
			<img class="img_quytrinh" src="./images/thiet_ke.png" alt="">
			<p>3.Thiết kế</p>
		</div>
		<div class="text-center col-xs-6 col-md-2 item-process wow swing">
			<img class="img_quytrinh" src="./images/hop_dong.png" alt="">
			<p>4.Hợp Đồng</p>
		</div>
		<div class="text-center col-xs-6 col-md-2 item-process wow swing">
			<img class="img_quytrinh" src="./images/thi_cong.png" alt="">
			<p>5.Thi công</p>
		</div>
		<div class="text-center col-xs-6 col-md-2 item-process wow swing">
			<img class="img_quytrinh" src="./images/ban_giao.png" alt="">
			<p>6.Bàn giao</p>
		</div>
	</div> 	
</div>