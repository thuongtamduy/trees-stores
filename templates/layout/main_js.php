



<script type="text/javascript" src="js/jquery.min.js"></script>

<script type="text/javascript" src="js/my_script.js"></script>
<script src="js/plugins-scroll.js" type="text/javascript" ></script>
<link href="fontawesome/css/font-awesome.css" type="text/css" rel="stylesheet" />
<script src="./bootstrap/js/jquery.min.js"></script>
<script src="./bootstrap/js/popper.min.js"></script>
<script src="./bootstrap/js/bootstrap.min.js"></script>

<!--js_assets-->
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
<script src="assets/js/core.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/jquery.prettyPhoto.js"></script>
<script src="assets/js/swiper.js"></script>
<script src="assets/js/rating.js"></script>
<script src="assets/js/jssor.slider.min.js"></script>
<script src="assets/js/fancybox.js"></script>
<script src="assets/js/_script2cc1.js?v=1584083170"></script>
<script
        type="text/javascript"> function GTranslateGetCurrentLang() { var keyValue = document['cookie'].match('(^|;) ?googtrans=([^;]*)(;|$)'); return keyValue ? keyValue[2].split('index.html')[2] : null; }
    function GTranslateFireEvent(element, event) { try { if (document.createEventObject) { var evt = document.createEventObject(); element.fireEvent('on' + event, evt) } else { var evt = document.createEvent('HTMLEvents'); evt.initEvent(event, true, true); element.dispatchEvent(evt) } } catch (e) { } }
    function doGTranslate(lang_pair) { if (lang_pair.value) lang_pair = lang_pair.value; if (lang_pair == '') return; var lang = lang_pair.split('|')[1]; if (GTranslateGetCurrentLang() == null && lang == lang_pair.split('|')[0]) return; var teCombo; var sel = document.getElementsByTagName('select'); for (var i = 0; i < sel.length; i++)if (/goog-te-combo/.test(sel[i].className)) { teCombo = sel[i]; break; } if (document.getElementById('google_translate_element2') == null || document.getElementById('google_translate_element2').innerHTML.length == 0 || teCombo.length == 0 || teCombo.innerHTML.length == 0) { setTimeout(function () { doGTranslate(lang_pair) }, 500) } else { teCombo.value = lang; GTranslateFireEvent(teCombo, 'change'); GTranslateFireEvent(teCombo, 'change') } }</script>

<!--js_assets-->

<!--Menu mobile-->
<script type="text/javascript" src="js/jquery.mmenu.min.all.js"></script>
<script type="text/javascript">
	$(function() {

		$m = $('nav#menu').html();
		$('nav#menu_mobi').append($m);
		
		$('.hien_menu').click(function(){
			$('nav#menu_mobi').css({height: "auto"});
		});
		$('.user .fa-user-plus').toggle(function(){
			$('.user ul').slideDown(300);
		},function(){
			$('.user ul').slideUp(300);
		});
		
		$('nav#menu_mobi').mmenu({
				extensions	: [ 'effect-slide-menu', 'pageshadow' ],
				searchfield	: true,
				counters	: true,
				navbar 		: {
					title		: 'Menu'
				},
				navbars		: [
					{
						position	: 'top',
						content		: [ 'searchfield' ]
					}, {
						position	: 'top',
						content		: [
							'prev',
							'title',
							'close'
						]
					}, {
						position	: 'bottom',
						content		: [
							'<a>Online : <?php $count=count_online();echo $tong_xem=$count['dangxem'];?></a>',
							'<a><?=_tong?> : <?php $count=count_online();echo $tong_xem=$count['daxem'];?></a>'
						]
					}
				]
			});
	});
</script>
<!--Menu mobile-->

<!--Hover menu-->
<script language="javascript" type="text/javascript">
	$(document).ready(function() { 
		//Hover vào menu xổ xuống
		$(".menu ul li").hover(function(){
			$(this).find('ul:first').css({visibility: "visible",display: "none"}).show(300); 
			},function(){ 
			$(this).find('ul:first').css({visibility: "hidden"});
			}); 
		$(".menu ul li").hover(function(){
				$(this).find('>a').addClass('active2'); 
			},function(){ 
				$(this).find('>a').removeClass('active2'); 
		}); 
		//Hover vào danh mục xổ xuống
		/*$("#danhmuc ul li").hover(function(){
			$(this).find('ul:first').show(300); 
		},function(){ 
			$(this).find('ul:first').hide(300);
		}); */
		//Click vào danh mục xổ xuống
		$("#danhmuc > ul > li > a").click(function(){
			if($(this).parents('li').children('ul').find('li').length>0)
			{
				$("#danhmuc ul li ul").hide(300);
				
				if($(this).hasClass('active'))
				{
					$("#danhmuc ul li a").removeClass('active');
					$(this).parents('li').find('ul:first').hide(300); 
					$(this).removeClass('active');
				}
				else
				{
					$("#danhmuc ul li a").removeClass('active');
					$(this).parents('li').find('ul:first').show(300); 
					$(this).addClass('active');
				}
				return false;
			}
		});
	});  
</script>
<!--Hover menu-->

<script type="text/javascript" src="js/slick.min.js"></script>


<!--Thêm alt cho hình ảnh-->
<script type="text/javascript">
	$(document).ready(function(e) {
        $('img').each(function(index, element) {
			if(!$(this).attr('alt') || $(this).attr('alt')=='')
			{
				$(this).attr('alt','<?=$company['ten']?>');
			}
        });
    });
</script>
<!--Thêm alt cho hình ảnh-->

<!--Tim kiem-->
<script type="text/javascript"> 
    function doEnter(evt){
		var key;
		if(evt.keyCode == 13 || evt.which == 13){
			onSearch(evt);
		}
	}
	function onSearch(evt) {			
	
			var keyword = document.getElementById("keyword").value;
			if(keyword=='' || keyword=='<?=_nhaptukhoatimkiem?>...')
			{
				alert('<?=_chuanhaptukhoa?>');
			}
			else{
				location.href = "tim-kiem.html&keyword="+keyword;
				loadPage(document.location);			
			}
		}		
</script>   
<!--Tim kiem-->


<script type="text/javascript">
    $(document).ready(function(){
      $('#slick_doitac').slick({
        	infinite: true,//Lặp lại
			accessibility:true,
		  	slidesToScroll: 1, //Số item cuộn khi chạy
		  	autoplay:true,  //Tự động chạy
			autoplaySpeed:3000,  //Tốc độ chạy
			speed:1000,//Tốc độ chuyển slider
			arrows:false, //Hiển thị mũi tên
			centerMode:false,  //item nằm giữa
			dots:false,  //Hiển thị dấu chấm
			draggable:true,  //Kích hoạt tính năng kéo chuột
			mobileFirst:true,
			pauseOnHover:true,
			//variableWidth: true//Không fix kích thước
			responsive: [
				{
				  breakpoint:1200,
				  settings: {
					slidesToShow: 6,
				  }
				},
				{
				  breakpoint:999,
				  settings: {
					slidesToShow: 5,
				  }
				},
				{
				  breakpoint:820,
				  settings: {
					slidesToShow: 4,
				  }
				},
				{
				  breakpoint:620,
				  settings: {
					slidesToShow: 3,
				  }
				},
				{
				  breakpoint:320,
				  settings: {
					slidesToShow: 2,
				  }
				}
			  ]
      });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
      $('#slick_dichvu').slick({
        	infinite: true,//Lặp lại
			accessibility:true,
		  	slidesToScroll: 1, //Số item cuộn khi chạy
		  	autoplay:true,  //Tự động chạy
			autoplaySpeed:5000,  //Tốc độ chạy
			speed:1000,//Tốc độ chuyển slider
			arrows:false, //Hiển thị mũi tên
			centerMode:false,  //item nằm giữa
			dots:false,  //Hiển thị dấu chấm
			draggable:true,  //Kích hoạt tính năng kéo chuột
			mobileFirst:true,
			pauseOnHover:true,
			//variableWidth: true//Không fix kích thước
			responsive: [
				{
				  breakpoint:1200,
				  settings: {
					slidesToShow: 4,
				  }
				},
				{
				  breakpoint:920,
				  settings: {
					slidesToShow: 3,
				  }
				},
				{
				  breakpoint:620,
				  settings: {
					slidesToShow: 2,
				  }
				}
				,
				{
				  breakpoint:320,
				  settings: {
					slidesToShow: 2,
				  }
				}
			  ]
      });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
      $('#slick_visao').slick({
        	infinite: true,//Lặp lại
			accessibility:true,
		  	slidesToScroll: 1, //Số item cuộn khi chạy
		  	autoplay:true,  //Tự động chạy
			autoplaySpeed:5000,  //Tốc độ chạy
			speed:1000,//Tốc độ chuyển slider
			arrows:false, //Hiển thị mũi tên
			centerMode:false,  //item nằm giữa
			dots:false,  //Hiển thị dấu chấm
			draggable:true,  //Kích hoạt tính năng kéo chuột
			mobileFirst:true,
			pauseOnHover:true,
			//variableWidth: true//Không fix kích thước
			responsive: [
				{
				  breakpoint:1200,
				  settings: {
					slidesToShow:3,
				  }
				},
				{
				  breakpoint:820,
				  settings: {
					slidesToShow: 2,
				  }
				},
				{
				  breakpoint:320,
				  settings: {
					slidesToShow: 2,
				  }
				}
			  ]
      });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
      $('#slick_slide2 , #slick_slide3, #slick_slide_index').slick({
        	infinite: true,//Lặp lại
			accessibility:true,
			slidesToShow: 1,
		  	slidesToScroll: 1, //Số item cuộn khi chạy
		  	autoplay:true,  //Tự động chạy
			autoplaySpeed:5000,  //Tốc độ chạy
			speed:1000,//Tốc độ chuyển slider
			arrows:false, //Hiển thị mũi tên
			centerMode:false,  //item nằm giữa
			dots:false,  //Hiển thị dấu chấm
			draggable:true,  //Kích hoạt tính năng kéo chuột
			mobileFirst:true,
			pauseOnHover:true,
			//variableWidth: true//Không fix kích thước
			
      });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
      $('#slick_slide_index2').slick({
        	infinite: true,//Lặp lại
			accessibility:true,
		  	slidesToScroll: 1, //Số item cuộn khi chạy
		  	autoplay:true,  //Tự động chạy
			autoplaySpeed:5000,  //Tốc độ chạy
			speed:1000,//Tốc độ chuyển slider
			arrows:false, //Hiển thị mũi tên
			centerMode:false,  //item nằm giữa
			dots:false,  //Hiển thị dấu chấm
			draggable:true,  //Kích hoạt tính năng kéo chuột
			mobileFirst:true,
			pauseOnHover:true,
			//variableWidth: true//Không fix kích thước
			responsive: [
				{
				  breakpoint:1200,
				  settings: {
					slidesToShow: 2,
				  }
				},
				{
				  breakpoint:620,
				  settings: {
					slidesToShow: 1,
				  }
				}
				
			  ]
			
      });
    });
</script>


<script type="text/javascript">
    $(document).ready(function(){
      $('#slick_tintuc_i').slick({
        	infinite: true,//Lặp lại
			vertical:true,//Chay dọc
			accessibility:true,
		  	slidesToScroll: 1, //Số item cuộn khi chạy
            slidesToShow: 3,
		  	autoplay:true,  //Tự động chạy
			autoplaySpeed:4000,  //Tốc độ chạy
			speed:1000,//Tốc độ chuyển slider
			arrows:false, //Hiển thị mũi tên
			centerMode:false,  //item nằm giữa
			dots:false,  //Hiển thị dấu chấm
			draggable:true,  //Kích hoạt tính năng kéo chuột
			mobileFirst:true,
			pauseOnHover:true,
			//variableWidth: true//Không fix kích thước
			
			
      });
    });
</script>

<script type="text/javascript">
$(document).ready(function(e) {
    $(window).scroll(function(){
        if(!$('.load_video').hasClass('load_video2'))
        {
            $('.load_video').addClass('load_video2');
            $('.load_video').load( "ajax/video.php");
        }
     });
});
</script>


<?php if($template=='index') { ?>
<script async="async" type="text/javascript" src="engine1/wowslider.js"></script>
<script async="async" type="text/javascript" src="engine1/script.js"></script>
<!-- End WOWSlider.com BODY section -->

<?php } ?>


<?php if($template=='product_detail') { ?>
<!-- slick -->
<script type="text/javascript">
    $(document).ready(function(){
		$('.slick2').slick({
			  slidesToShow: 1,
			  slidesToScroll: 1,
			  arrows: false,
			  fade: true,
			  autoplay:false,  //Tự động chạy
			  autoplaySpeed:5000,  //Tốc độ chạy
			  asNavFor: '.slick'			 
		});
		$('.slick').slick({
			  slidesToShow: 4,
			  slidesToScroll: 1,
			  asNavFor: '.slick2',
			  dots: false,
			  centerMode: false,
			  focusOnSelect: true
		});
		 return false;
    });
</script>
<!-- slick -->

<script src="magiczoomplus/magiczoomplus.js" type="text/javascript"></script>
<script type="text/javascript">
	var mzOptions = {
		zoomMode:true,
		onExpandClose: function(){MagicZoom.refresh();}
	};
</script>




<?php } ?>


<script language="javascript" type="text/javascript">
	$(document).ready(function(){
		$('#content_tabs .tab').hide();
		$('#content_tabs .tab:first').show();
		$('#ultabs li:first').addClass('active');
		
		$('#ultabs li').click(function(){
			var vitri = $(this).data('vitri');
			$('#ultabs li').removeClass('active');
			$(this).addClass('active');
			
			$('#content_tabs .tab').hide();
			$('#content_tabs .tab:eq('+vitri+')').show();
			return false;
		});	
	});
</script>
<!--Tags sản phẩm-->


<script type="text/javascript">
	$(document).ready(function(e) {

       $('a.xem_video').click(function(){
			var link_viveo = $(this).attr('href');
			$('#view_video').append('<div class="login-popup"><div class="close-popup">x</div><div class="video_popup"><iframe width="100%" height="480px" src="https://www.youtube.com/embed/'+link_viveo+'?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe></div></div>');
			$('.login-popup').fadeIn(300);
						
			var chieucao = $('.login-popup').height() / 2;
			var chieurong = $('.login-popup').width() /2;
			$('.login-popup').css({'margin-top':-chieucao,'margin-left':-chieurong});
			$('#view_video').append('<div id="baophu"></div>');
			$('#baophu').fadeIn(300);

			return false;
			
		});

		$('#baophu, .close-popup').click(function(){
			$('#baophu, .login-popup').fadeOut(300, function(){
			$('#baophu').remove();
			$('.login-popup').remove();
			
			});
		});
		
    });
</script>


<?php if($com=='lien-he') { ?>
<script type="text/javascript">
	$(document).ready(function(e) {
		$('.click_ajax').click(function(){
			if(isEmpty($('#ten_lienhe').val(), "<?=_nhaphoten?>"))
			{
				$('#ten_lienhe').focus();
				return false;
			}
			
			if(isEmpty($('#dienthoai_lienhe').val(), "<?=_nhapsodienthoai?>"))
			{
				$('#dienthoai_lienhe').focus();
				return false;
			}
			if(isPhone($('#dienthoai_lienhe').val(), "<?=_nhapsodienthoai?>"))
			{
				$('#dienthoai_lienhe').focus();
				return false;
			}
			if(isEmpty($('#email_lienhe').val(), "<?=_emailkhonghople?>"))
			{
				$('#email_lienhe').focus();
				return false;
			}
			if(isEmail($('#email_lienhe').val(), "<?=_emailkhonghople?>"))
			{
				$('#email_lienhe').focus();
				return false;
			}
			
			if(isEmpty($('#noidung_lienhe').val(), "<?=_nhapnoidung?>"))
			{
				$('#noidung_lienhe').focus();
				return false;
			}
			
			frm.submit();
		});    
    });
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#reset_capcha").click(function() {
			$("#hinh_captcha").attr("src", "sources/captcha.php?"+Math.random());
			return false;
		});
	});
</script>

<?php } ?>





<?php if($com=='gio-hang') { ?>
<script type="text/javascript">
	function del(pid,size,mausac){
		if(confirm('Do you really mean to delete this item')){
			document.form1.pid.value=pid;
			document.form1.size.value=size;
			document.form1.mausac.value=mausac;
			document.form1.command.value='delete';
			document.form1.submit();
		}
	}
	function clear_cart(){
		if(confirm('This will empty your shopping cart, continue?')){
			document.form1.command.value='clear';
			document.form1.submit();
		}
	}
	function update_cart(){
		document.form1.command.value='update';
		document.form1.submit();
	}
	function quaylai()
	{
		history.go(-1);
	}
	
	function doEnter_update(evt){
		var key;
		if(evt.keyCode == 13 || evt.which == 13){
			update_cart(evt);
		}
	}
</script>


<script type="text/javascript">
	$(document).ready(function(e) {
		
		$('.click_ajax2').click(function(){
			if(isEmpty($('#httt').val(), "<?=_chonhinhthucthanhtoan?>"))
			{
				$('#httt').focus();
				return false;
			}
			if(isEmpty($('#hoten').val(), "<?=_nhaphoten?>"))
			{
				$('#hoten').focus();
				return false;
			}
			if(isEmpty($('#dienthoai').val(), "<?=_nhapsodienthoai?>"))
			{
				$('#dienthoai').focus();
				return false;
			}
			if(isPhone($('#dienthoai').val(), "<?=_nhapsodienthoai?>"))
			{
				$('#dienthoai').focus();
				return false;
			}

			if(isEmpty($('#diachi').val(), "<?=_nhapdiachi?>"))
			{
				$('#diachi').focus();
				return false;
			}
			
			if(isEmpty($('#email_lienhe').val(), "<?=_emailkhonghople?>"))
			{
				$('#email_lienhe').focus();
				return false;
			}
			if(isEmpty($('#noidung').val(), "<?=_nhapnoidung?>"))
			{
				$('#noidung').focus();
				return false;
			}
			frm_order.submit();
		});    
    });
</script>

<?php } ?>


<script type="text/javascript">
	$(document).ready(function(){
		$(window).scroll(function(){
			var cach_top = $(window).scrollTop();
			var heaigt_header = $('#header').height();

			if(cach_top >= heaigt_header){
				$('div.wap_menu').css({position: 'fixed', top: '0px', zIndex:99999});
				
			}else{
				$('div.wap_menu').css({position: 'relative' , zIndex:99});

			}
		});
	});
 </script>



<script type="text/javascript" src="js/jQuery.WCircleMenu-min.js"></script>
<script>
	$(document).ready(function(){
		$('#my-menu').WCircleMenu({
			angle_start : -Math.PI,
			delay: 50,
			distance: 80,
			angle_interval: Math.PI/4,
			easingFuncShow:"easeOutBack",
			easingFuncHide:"easeInBack",
			step:5,
			openCallback:false,
			closeCallback:false,
			itemRotation:180,
			iconRotation:180,
		});
	})

</script>


<script type="text/javascript">

	$(document).ready(function(){
		setTimeout(function(){ 
			$('.img_slide').fadeOut(700);
			$('.load_video_desk').fadeIn(400);
		 }, 2000);

	})

	var tag = document.createElement('script');
		tag.src = 'https://www.youtube.com/player_api';
	var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	var tv,
		playerDefaults = {autoplay: 0, autohide: 1, modestbranding: 0, rel: 1, showinfo: 0, controls: 0, disablekb: 1, enablejsapi: 0, iv_load_policy: 3};
	var vid = [
			{'videoId': '<?=$url_video?>', 'startSeconds': 0,  'suggestedQuality': 'hd720'}
		],
		randomVid = Math.floor(Math.random() * vid.length),
	currVid = randomVid;

	$('.hi em:last-of-type').html(vid.length);

	function onYouTubePlayerAPIReady(){
	tv = new YT.Player('tv', {events: {'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange}, playerVars: playerDefaults});
	}

	function onPlayerReady(){
	tv.loadVideoById(vid[currVid]);
	tv.mute();
	}

	function onPlayerStateChange(e) {
	if (e.data === 1){
	$('#tv').addClass('active');
	$('.hi em:nth-of-type(2)').html(currVid + 1);
	} else if (e.data === 2){
	$('#tv').removeClass('active');
	if(currVid === vid.length - 1){
	  currVid = 0;
	} else {
	  currVid++;  
	}
	tv.loadVideoById(vid[currVid]);
	tv.seekTo(vid[currVid].startSeconds);
	}
	}

	function vidRescale(){

	var w = $(window).width()+10,
	h = $(window).height()+10;

	if (w/h > 16/9){
	tv.setSize(w, w/16*9);
	$('.tv .screen').css({'left': '0px'});
	} else {
	tv.setSize(h/9*16, h);
	$('.tv .screen').css({'left': -($('.tv .screen').outerWidth()-w)/2});
	}
	}

	$(window).on('load resize', function(){
	vidRescale();
	});

	$('.hi span:first-of-type').on('click', function(){
	$('#tv').toggleClass('mute');
	$('.hi em:first-of-type').toggleClass('hidden');
	if($('#tv').hasClass('mute')){
	tv.mute();
	} else {
	tv.unMute();
	}
	});

	$('.hi span:last-of-type').on('click', function(){
	$('.hi em:nth-of-type(2)').html('~');
	tv.pauseVideo();
	});

    $(document).ready(function() {

        var owl1 = $("#owl-demo-product1");
        var owl2 = $("#owl-demo-product2");
        var owl3 = $("#owl-demo-product3");
        var owl4 = $("#owl-demo-product4");
        var owl5 = $("#owl-demo-product5");
        owl1.owlCarousel({
            items : 5,
            itemsDesktop : [1199,5],
            responsive:{
                0:{
                    items:1.5,
                    nav:true,
                    navText: false,
                    loop:true
                },
                768:{
                    items:3,
                    nav:true,
                    navText: false,
                    loop:true
                },
                1000:{
                    items:5,
                    nav:true,
                    navText: false,
                    loop:true
                }
            },
            singleItem : false,
            autoplay:true,
            autoplayHoverPause:false,
            autoplayTimeout: 2000
        });
        owl2.owlCarousel({
            items : 5,
            itemsDesktop : [1199,5],
            responsive:{
                0:{
                    items:1.5,
                    nav:true,
                    navText: false,
                    loop:true
                },
                768:{
                    items:3,
                    nav:true,
                    navText: false,
                    loop:true
                },
                1000:{
                    items:5,
                    nav:true,
                    navText: false,
                    loop:true
                }
            },
            singleItem : false,
            autoplay:true,
            autoplayHoverPause:false,
            autoplayTimeout: 2000
        });
        owl3.owlCarousel({
            items : 5,
            itemsDesktop : [1199,5],
            responsive:{
                0:{
                    items:1.5,
                    nav:true,
                    navText: false,
                    loop:true
                },
                768:{
                    items:3,
                    nav:true,
                    navText: false,
                    loop:true
                },
                1000:{
                    items:5,
                    nav:true,
                    navText: false,
                    loop:true
                }
            },
            singleItem : false,
            autoplay:true,
            autoplayHoverPause:false,
            autoplayTimeout: 2000
        });
        owl4.owlCarousel({
            items : 5,
            itemsDesktop : [1199,5],
            responsive:{
                0:{
                    items:1,
                    nav:true,
                    navText: false,
                    loop:true
                },
                768:{
                    items:3,
                    nav:true,
                    navText: false,
                    loop:true
                },
                1000:{
                    items:5,
                    nav:true,
                    navText: false,
                    loop:true
                }
            },
            singleItem : false,
            autoplay:true,
            autoplayHoverPause:false,
            autoplayTimeout: 3000
        });
        owl5.owlCarousel({
            items : 5,
            itemsDesktop : [1199,5],
            responsive:{
                0:{
                    items:1,
                    nav:true,
                    navText: false,
                    loop:true
                },
                768:{
                    items:3,
                    nav:true,
                    navText: false,
                    loop:true
                },
                1000:{
                    items:5,
                    nav:true,
                    navText: false,
                    loop:true
                }
            },
            singleItem : false,
            autoplay:true,
            autoplayHoverPause:false,
            autoplayTimeout: 3000
        });

        var swiper = $("#swiper-container1");
        swiper = new Swiper('.swiper-container', {
            autoplay: {
                delay: 4000
            },
            spaceBetween: 10,
            loop: true,
            loopFillGroupWithBlank: true,
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            }
        });
    });
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/<?php if($lang=='en')echo 'en_EN';else echo 'vi_VN' ?>/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--Meta facebook-->   

<script src="https://sp.zalo.me/plugins/sdk.js"></script>


<script src='https://www.google.com/recaptcha/api.js'></script>


<?php /*?>

<!--lockfixed
<script type="text/javascript" src="js/jquery.lockfixed.min.js"></script>
<script type="text/javascript">
	$(window).load(function(e) {
		(function($) {
				var left_h=$('#left').height();
				var main_h=$('#main').height();
				if(main_h>left_h) 
				{
					$.lockfixed("#scroll-left",{offset: {top: 10, bottom: 400}});
				}
		})(jQuery);
	});
</script>
<!--lockfixed-->

