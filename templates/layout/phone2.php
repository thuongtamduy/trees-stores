<?php if($deviceType!='computer') { ?>

<div class="widget-mobile">
    <div id="my-menu">
        <div class="wcircle-icon" style="position: absolute; top: 0px; height: 50px; width: 50px; display: block;"><i class="fa fa-phone" aria-hidden="true"></i></div>
        <div class="wcircle-menu" style="width: 50px; height: 50px; position: relative; display: none;">
            <div class="wcircle-menu-item" style="position: absolute; top: 0px; left: 0px; opacity: 0; transform: translate3d(-80px, 0px, 0px) rotate(-270deg);">
                <a href="tel:<?=preg_replace('/[^0-9]/','',$company['dienthoai']);?>"><i class="fa fa-phone" style="transform: rotate(-270deg); "></i></a>
            </div>
            <div class="wcircle-menu-item" style="position: absolute; top: 0px; left: 0px; opacity: 0;">
                <a href="sms:<?=preg_replace('/[^0-9]/','',$company['dienthoai']);?>"><i class="fa fa-comments"></i></a>
            </div>
            <div class="wcircle-menu-item" style="position: absolute; top: 0px; left: 0px; opacity: 0;">
                <a target="_blank" href="lien-he.html"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
            </div>
            <div class="wcircle-menu-item" style="position: absolute; top: 0px; left: 0px; opacity: 0;">
                <a target="_blank" href="<?=$company['fanpage']?>"><img src="images/mess.png" alt="Zalo"></a>
            </div>
            <div class="wcircle-menu-item" style="position: absolute; top: 0px; left: 0px; opacity: 0;">
                <a target="_blank" href="https://zalo.me/<?=$company['zalo']?>"><img src="images/zalo.png" alt="Zalo"></a>
            </div>
        </div>
    </div>
</div>

<style>

    div.widget-mobile { position: fixed; left: 50%; transform: translateX(-50%); bottom: 10px; z-index: 9999999; display: block; }
    div#my-menu { position: relative; width: 50px!important; height: 50px!important; }
    div.wcircle-open .wcircle-icon i:before { content: '\f00d'; }
    div.wcircle-icon { background: #1282fc;  border-radius: 50%; display: flex!important; display: -ms-flex!important; align-items: center; -ms-flex-align: center; -webkit-box-pack: center; -ms-flex-pack: center; justify-content: center; position: relative!important; }
    div.wcircle-icon:before { position: absolute; content: ''; width: 60px; height: 60px; background: rgba(18,130,252,.5); border: 1px solid #fff; border-radius: 50%; left: -5px; top: -5px; -webkit-animation: pulse 1s infinite ease-in-out; -moz-animation: pulse 1s infinite ease-in-out; -ms-animation: pulse 1s infinite ease-in-out; -o-animation: pulse 1s infinite ease-in-out; animation: pulse 1s infinite ease-in-out; }
    div.wcircle-icon:after { position: absolute; content: ''; width: 80px; height: 80px; background: rgba(18,130,252,.5); border-radius: 50%; left: -15px; top: -15px; -webkit-animation: zoomIn 2s infinite ease-in-out; -moz-animation: zoomIn 2s infinite ease-in-out; -ms-animation: zoomIn 2s infinite ease-in-out; -o-animation: zoomIn 2s infinite ease-in-out; animation: zoomIn 2s infinite ease-in-out; }
    div.wcircle-menu { position: absolute!important; left: 0; top: 0; display: none; }
    div.wcircle-menu-item { width: 50px; height: 50px; background: #1282fc; border-radius: 50%; display: flex; display: -ms-flex; align-items: center; -ms-flex-align: center; -webkit-box-pack: center; -ms-flex-pack: center; justify-content: center; }
    div.wcircle-menu-item img { width: 50px; height: 50px; display: block; border-radius: 50%; }
    div.wcircle-menu-item i, div.wcircle-icon i { font-size: 25px; color: #fff; position: relative; z-index: 9999; }
</style>



<?php } ?>