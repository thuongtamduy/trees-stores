<?php 

$d->reset();
$sql_tt = "select id,ten$lang as ten,tenkhongdau,thumb,photo,mota$lang as mota FROM #_news_danhmuc where type='dichvu' and noibat>0 and hienthi=1 order by stt asc limit 0,20";     
$d->query($sql_tt);
$dichvu_nb = $d->result_array();

?>

<div id="dichvu_noibat">

<div class="tieude_dichvu">
    <span>DỊCH VỤ CẢNH QUAN</span>
</div>

<div id="slick_dichvu">
  
    <?php foreach($dichvu_nb as $k => $value) { ?>
    <div class="box_dv_index">
        <a href="dich-vu/<?=$value['tenkhongdau']?>/" title="<?=$value['ten']?>">
        <img class="img" src="<?=_upload_tintuc_l.$value['thumb']?>" alt="<?=$value['ten']?>" />
        </a>
        
        <div class="ten"><a href="dich-vu/<?=$value['tenkhongdau']?>/" title="<?=$value['ten']?>" ><?=$value['ten']?></a></div>
        <div class="mota"><?=catchuoi($value['mota'],100)?></div>
        <a class="xemchitiet" href="dich-vu/<?=$value['tenkhongdau']?>/">XEM CHI TIẾT</a>
       
    </div><!---END .item-->
    <?php } ?>
</div>

</div>