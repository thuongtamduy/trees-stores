<?php


$d->reset();
$sql_tt = "select id,ten$lang as ten,tenkhongdau,thumb,photo,mota$lang as mota,ngaytao FROM #_news where  noibat>0 and hienthi=1 order by stt asc limit 0,20";
$d->query($sql_tt);
$tintuc_i = $d->result_array();

?>
<section class="container-fluid container-wrap home-blog home-blog1" id="home-blog">
    <div class="row">
        <div class="col-md-12 title wow bounceInUp" data-wow-delay="1s">
            <p><a href="#" style="color: #ff3b00;">DỰ ÁN THI CÔNG</a></p>
        </div>
        <div class=" wow slideInUp col-md-12 home-blog-row owl-carousel">
            <?php foreach($tintuc_i as $k => $value){   ?>
            <div class="item-blog wow pulse">
                <div class="featured-img">
                    <a href=tin-tuc/<?=$value['tenkhongdau']?>.html" title="<?=$value['ten']?>">
                        <img src="thumb/370x200/1/<?=_upload_tintuc_l.$value['photo']?>" alt="<?=$value['ten']?>">
                    </a>
                </div>
                <div class="content-blog">
                    <a href="tin-tuc/<?=$value['tenkhongdau']?>.html" class="title"  ><?=$value['ten']?></a>
                    <p class="height50"><?=catchuoi($value['mota'],100)?></p>
                    <p class="readmore"><a href="tin-tuc/<?=$value['tenkhongdau']?>.html" >Xem thêm</a></p>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>