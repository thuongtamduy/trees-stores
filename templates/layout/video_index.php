
<?php 
$d->reset();
$sql_video = "select id,ten$lang as ten,link from #_video where hienthi=1 and type='video' order by stt,id desc";
$d->query($sql_video);
$video_index = $d->result_array();
?>

<div id="video_index">

	<div class="box_container">
		<div class="tieude_index">
		<span>VIDEO CLIP</span>
		</div>

		<ul id="slick_video">
				<?php foreach($video_index as $k => $value) { ?>
				<li class="item_video">
					<p class="img">
					<a class="btn_xem_video" href="<?=getYoutubeIdFromUrl($value['link'])?>" title="<?=$value['ten']?>"></a>
					<img src="http://img.youtube.com/vi/<?=getYoutubeIdFromUrl($value['link'])?>/hqdefault.jpg" alt="<?=$value['ten']?>" />
					</p>

					<p class="ten"><?=$value['ten']?></p>
				</li><!---END .item-->
				<?php } ?>
		</ul>

	</div>

</div>

<div id="view_video"></div>

<style type="text/css">
	
#slick_video{list-style:none;padding:0;margin:0 auto;max-width:70%;margin: 0px auto;
	position:relative;z-index:0;width:100%;font-family: OpenSans_R;font-size: 15px;text-align: center;
height: 420px;
}

.item_video {max-height:420px;width:75%;display: inline-block;vertical-align: top;text-align: center;
	position: relative;
}
.item_video .img {width:100%;display: inline-block;vertical-align: top;}
.item_video .info {width: 100%;display: inline-block;vertical-align: top;opacity:0;transition:.5s;font-size: 16px;
color: #F4F4F4;padding-top: 10px;}
.item_video .ten{width:100%;display:inline-block;vertical-align:top;padding:5px;text-align:center;
	font-family:Roboto_M;font-size:18px;color:#333;font-weight:400;opacity: 0;}

.roundabout-in-focus .ten{opacity: 1;transition: 0.3s;}

.item_video .btn_xem_video {width: 60px;height: 42px;position: absolute;top: calc(50% - 21px);

left: calc(50% - 30px);z-index: 9999;
background: url(images/icon-play-yt.png) no-repeat center;
cursor: pointer;opacity: 0.8;transition: 0.4s;}
.item_video .btn_xem_video:hover {opacity: 1;transition: 0.4s;}
</style>


<script type="text/javascript" src="js/jquery.roundabout.js"></script>
<script type="text/javascript" src="js/jquery.roundabout-shapes.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$('#slick_video').roundabout({         
		duration:1200,
		minScale:0.6,
		minOpacity:1,
		maxOpacity:1,
		autoplay:false,
		autoplayInitialDelay:5000,
		autoplayDuration:4000,
		autoplayPauseOnHover:true,
		reflect: true
	});
});
   
</script> 




<script type="text/javascript">
	$(document).ready(function(e) {
        $('a.btn_xem_video , a.xem_video').click(function(){
			var link_viveo = $(this).attr('href');

			$('#view_video').append('<div class="login-popup"><div class="close-popup">x</div><div class="video_popup"><iframe width="100%" height="400px" src="https://www.youtube.com/embed/'+link_viveo+'?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe></div></div>');
			$('.login-popup').fadeIn(300);
						
			var chieucao = $('.login-popup').height() / 2;
			var chieurong = $('.login-popup').width() /2;
			$('.login-popup').css({'margin-top':-chieucao,'margin-left':-chieurong});
			$('#view_video').append('<div id="baophu"></div>');
			$('#baophu').fadeIn(300);
			return false;
			
		});
		$('#baophu, .close-popup').click(function(){
			$('#baophu, .login-popup').fadeOut(300, function(){
				$('#baophu').remove();
				$('.login-popup').remove();
			});		
			
		});

    });
</script>
