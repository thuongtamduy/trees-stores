<?php
#gioi_thieu
$sql = "select ten$lang as ten,mota$lang as mota,title,keywords,description,photo from #_about where type='about' and hienthi=1 limit 0,1";
$d->query($sql);
$about_index = $d->fetch_array();

$d->reset();
$sql_tt = "select id,ten$lang as ten,tenkhongdau,thumb,photo,mota$lang as mota,ngaytao FROM #_news where type='tieuchi' and hienthi=1 order by stt asc limit 0,20";
$d->query($sql_tt);
$tieuchi_i = $d->result_array();

?>
<div id="gioithieu_index">

    <div class=" content_noidung wow fadeInDown" data-wow-delay="300ms">
        <div class="col_w50">
            <div class="content_gioithieu">
                <div class="title_gioithieu2">GIỚI THIỆU </div>
                <div class="title_gioithieu">
                    <?= $about_index['ten'] ?></div>
                <?= $about_index['mota'] ?>
            </div>
            <div id="slick_visao">
                <?php foreach ($tieuchi_i as $k => $value) { ?>
                    <div class="item_vs">
                        <a href="tieu-chi/<?= $value['tenkhongdau'] ?>.html" title="<?= $value['ten'] ?>">
                            <img class="img" src="thumb/50x50/2/<?= _upload_tintuc_l . $value['photo'] ?>"
                                 alt="<?= $value['ten'] ?>"/>
                        </a>

                        <div class="ten "><a href="tieu-chi/<?= $value['tenkhongdau'] ?>.html"
                                            title="<?= $value['ten'] ?>"><?= $value['ten'] ?></a></div>
                        <p class="mota"><?= catchuoi($value['mota'], 40) ?></p>
                    </div><!---END .item-->
                <?php } ?>
            </div>
            <a href="gioi-thieu.html" class="link-xemthem2"><?= _xemthem ?></a>
        </div>
        <div class="col_w50">
            <!--                <img class="img_about" src="-->
            <? //= _upload_hinhanh_l . $about_index['photo'] ?><!--"-->
            <!--                     alt="--><? //= $about_index['title'] ?><!--"/>-->
            <div class="container-fluid container-wrap home-about">
                <div class="content-right wow bounceInUp" data-de>
                    <div class="box box-1">
                        <div class="featured-icon"><img src="images/cau_truc_ben_vung.png" alt="about icon 1"/></div>
                        <div class="featured-content">
                            <h3>Cấu Trúc Bền Vững</h3>
                            <p>Chúng tôi luôn đem đến cho khách hàng và các đối tác một sự yên tâm tuyệt đối với các
                                công trình sân
                                vườn. Công trình của chúng tôi thực hiện luôn đạt tiêu chuẩn kỹ thuật chuyên nghiệp và
                                khắt khe nhất, sử
                                dụng các thiết bị máy móc chính hãng bảo hành lâu dài.</p>
                        </div>
                    </div>
                    <div class="box box-2">
                        <div class="featured-icon"><img src="images/tinh_tham_my_cao.png" alt="about icon 2"/></div>
                        <div class="featured-content">
                            <h3>Tính Thẫm Mỹ Cao</h3>
                            <p>Thẩm mỹ luôn là là yếu tố quan trọng mà Sân Vườn Trúc Xinh luôn mang lại cho khách hàng,
                                Các công trình
                                luôn được nắm bắt ý tưởng , thiết kế phối cảnh chi tiết và tư vấn cho khách hàng lựa
                                chọn với nhiều
                                phương án khác nhau trước khi bắt tay vào khởi công công trình.</p>
                        </div>
                    </div>
                    <div class="box box-3">
                        <div class="featured-icon"><img src="images/tiet_kiem_chi_phi.png"  alt="about icon 3"/></div>
                        <div class="featured-content">
                            <h2>Tiết Kiệm Chi Phí</h2>
                            <section id="home-about">
                                <p>Không phải ai cũng có nguồn tài chính dư giả để thoải mái đầu tư cho các công trình
                                    thiết kế thi công
                                    cảnh quan sân vườn, do đó Trạng Nguyên luôn đem lại cho khách hàng nhiều
                                    phương án kính phí tiết
                                    kiệm và hợp lý với từng hạng mục của dự án.</p>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>

