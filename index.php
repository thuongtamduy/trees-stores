<?php

session_start();
$session=session_id();

@define ( '_template' , './templates/');
@define ( '_source' , './sources/');
@define ( '_lib' , './admin/lib/');

include_once _lib."Mobile_Detect.php";
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');

$lang="";

require_once _source."lang$lang.php";	
include_once _lib."config.php";
include_once _lib."constant.php";
include_once _lib."functions.php";
include_once _lib."class.database.php";
include_once _lib."functions_user.php";
include_once _lib."functions_giohang.php";
include_once _lib."file_requick.php";
include_once _source."counter.php";
?>

<!doctype html>
<html lang="vi">

<head itemscope itemtype="http://schema.org/WebSite" >
    
<base href="http://<?=$config_url?>/" />

 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0, user-scalable=yes">
<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->

<link rel="canonical" href="<?=getCurrentPageURL()?>" />

<?php include _template."layout/seoweb.php";?>
<?php include _template."layout/main_css.php";?> 

<?=$company['codethem']?>  
     
</head>

<?php //include _template."layout/background.php";?>


<body>
 
<div id="pre-loader"><div class="loader"></div></div>

<address class="vcard nodisplay">
    <span class="org"><?=$company['ten']?></span>,
    <span class="adr"></span>
    <span class="street-address"><?=$company['diachi']?></span>,
    <span class="locality">Ho Chi Minh</span>,
    <span class="postal-code">70000</span>,
    <span class="country-name">VN</span>.
    <span class="tel"><?=$company['dienthoai']?></span>
</address>

<div id="wapper">

    <div id="header">
        <?php include _template."layout/header.php";?>
    </div><!---END #header-->
        
    <div class="wap_menu">
            <div class="menu">
            <?php include _template."layout/menu_top.php";?>
            </div><!---END .menu-->
        </div><!---END .wap_menu--> 
    
    <?php if($template=='index') { ?>
    <div id="slider">
        <?php  if($deviceType=='computer') include _template."layout/full_video.php";
                else include _template."layout/slider.php"
         ?>
    </div><!---END #slider--> 
    <?PHP } ?>
<!-- --><?php //if($template=='index') { include _template."layout/dichvu_noibat.php"; } ?>

    <?php if($template=='index') include _template."layout/gioithieu_index.php"; ?>
    <?php if($template=='index') include _template."layout/dichvu_index.php"; ?>
    <?php if($template=='index') include _template."layout/quytrinh_index.php"; ?>
    <?php if($template=='index') include _template."layout/kinhnghiem_index.php"; ?>

    <div class="clear"></div>

    <div id="main_content">
        <?php include _template.$template."_tpl.php"; ?>
        <div class="clear"></div>
    </div><!---END #main_content-->
    <?php  include _template."layout/tintuc_index.php"; ?>
<!--    --><?php // include _template."layout/dangkynhantin.php"; ?>
<!--    --><?php // include _template."layout/doitac.php"; ?><!-- -->
    <?php if($template != 'index') include _template."layout/post-contact.php"; ?>
    <div class="clear"></div>
    
    <div id="wap_footer" >
        <?php include _template."layout/footer.php";?>
        <div class="clear"></div>
    </div><!---END#wap_footer-->

    
</div><!---END #wapper-->


<?php include _template."layout/main_js.php";?>

<?php include _template."layout/phone2.php";?>

<?php include _template."layout/main_js_dkdn.php";?>





<?php if($deviceType=='computer') { include _template."layout/chat_facebook.php"; } ?>


</body>
</html>

