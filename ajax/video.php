<?php 
	include ("ajax_config.php");
	
	$d->reset();
	$sql_video = "select id,ten$lang as ten,link from #_video where hienthi=1 and type='video' order by stt,id desc";
	$d->query($sql_video);
	$video = $d->result_array();
	
?>


<script type="text/javascript">
	$(document).ready(function(e) {
        $('.select_video .img_vd').click(function(){
			var src = 'https://www.youtube.com/embed/'+$(this).attr('data-id');
			$('.left_video iframe').attr('src',src);
		});
    });
</script>


<?php if($deviceType=='computer') { ?>
<script type="text/javascript">
    $(document).ready(function(){
      $('#slick_video').slick({
		  	lazyLoad: 'ondemand',
        	infinite: true,//Hiển thì 2 mũi tên
			accessibility:true,
			vertical:true,//Chay dọc
		  	slidesToShow: 4,    //Số item hiển thị
		  	slidesToScroll: 1, //Số item cuộn khi chạy
		  	autoplay:true,  //Tự động chạy
			autoplaySpeed:3000,  //Tốc độ chạy
			speed:1000,
			arrows:false, //Hiển thị mũi tên
			centerMode:false,  //item nằm giữa
			dots:false,  //Hiển thị dấu chấm
			draggable:true,  //Kích hoạt tính năng kéo chuột
			mobileFirst:true
      });
	});
</script>

<style type="text/css">
.left_video {width:calc(100% - 120px);float: left;}
.select_video {width:110px;float: right;height: 365px;}
.select_video img {width:100%;cursor:pointer;margin-bottom: 10px;height: 82px;opacity: 0.8;transition: 0.4s;}
.select_video img:hover {opacity: 1;transition: 0.4s;}
</style>
    

<?php } else { ?>

<script type="text/javascript">
    $(document).ready(function(){
      $('#slick_video').slick({
		  	lazyLoad: 'ondemand',
        	infinite: true,//Hiển thì 2 mũi tên
			accessibility:true,
		  	slidesToShow: 4,    //Số item hiển thị
		  	slidesToScroll: 1, //Số item cuộn khi chạy
		  	autoplay:true,  //Tự động chạy
			autoplaySpeed:3000,  //Tốc độ chạy
			speed:1000,
			arrows:false, //Hiển thị mũi tên
			centerMode:false,  //item nằm giữa
			dots:false,  //Hiển thị dấu chấm
			draggable:true,  //Kích hoạt tính năng kéo chuột
			mobileFirst:true
      });
	});
</script>

<style type="text/css">
.left_video {width:100%;display: inline-block;verticaltop;}
.select_video {width:100%;display: inline-block;verticaltop;}
.select_video img {width:calc(100% - 2px);cursor:pointer;margin: 1px;opacity: 0.8;transition: 0.4s;}
.select_video img:hover {opacity: 1;transition: 0.4s;}
</style>


<?php } ?>


<div class="video_popup left_video">
<iframe title="<?=$video[0]['ten']?>" width="100%" height="360px" src="https://www.youtube.com/embed/<?=getYoutubeIdFromUrl($video[0]['link'])?>" frameborder="0" allowfullscreen></iframe>
</div>
    
<div class="select_video" id="slick_video">
<?php for($i=0;$i<count($video);$i++){?>
<img class="img_vd" data-id="<?=getYoutubeIdFromUrl($video[$i]['link'])?>" src="https://img.youtube.com/vi/<?=getYoutubeIdFromUrl($video[$i]['link'])?>/0.jpg" alt="<?=$product[$i]['ten']?>" title="<?=$product[$i]['ten']?>" />
<?php } ?>
</div>



