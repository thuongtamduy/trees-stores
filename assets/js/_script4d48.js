$(function(){
    new WOW().init();
	/*** scroll top ***/
	$(window).scroll(function () {
		$(this).scrollTop() > 100 ? $(".scroll-to-top").fadeIn() : $(".scroll-to-top").fadeOut()
	}), $(".scroll-to-top").click(function () {
		return $("html, body").animate({scrollTop:0}, 800), !1
	})
	
	/*** owl slideshow ***/
	$('.slideshow .owl-carousel').owlCarousel({
		loop:true,
		items:1,		
		autoplay:true,
		autoplayHoverPause:false,
        nav:true,
        navText: ['<','>'],
        animateOut: 'fadeOut'
	});

    $('.home-feedback .owl-carousel').owlCarousel({
        loop:true,
        responsive:{
            0:{
                items:1,
                nav:false,
                loop:true
            },
            600:{
                items:1,
                nav:false,
                loop:true
            },
            768:{
                items:2,
                nav:false,
                loop:true
            },
            1000:{
                items:2,
                nav:false,
                loop:true
            }
        },
        autoplay:true,
        autoplayHoverPause:false
    });

    $('.home-partsv .owl-carousel').owlCarousel({
        loop:true,
        responsive:{
            0:{
                items:1,
                nav:true,
                navText: ['<','>'],
                loop:true
            },
            600:{
                items:4,
                nav:true,
                navText: ['<','>'],
                loop:true
            },
            768:{
                items:6,
                nav:true,
                navText: ['<','>'],
                loop:true
            },
            1000:{
                items:5,
                nav:true,
                navText: ['<','>'],
                loop:true
            }
        },
        autoplay:true,
        autoplayHoverPause:false
    });

    $('.home-product2 .owl-carousel').owlCarousel({
        loop:true,
        responsive:{
            0:{
                items:1,
                nav:true,
                navText: ['<','>'],
                loop:true
            },
            600:{
                items:1,
                nav:true,
                navText: ['<','>'],
                loop:true
            },
            768:{
                items:1,
                nav:true,
                navText: ['<','>'],
                loop:true
            },
            1000:{
                items:1,
                nav:true,
                navText: ['<','>'],
                loop:true
            }
        },
        autoplay:true,
        autoplayHoverPause:false,
        animateOut: 'fadeOut'
    });

	/** TOGGLE MOBILE MENU **/ 
	var sideslider = $('[data-toggle=collapse-side]');
	var sel = sideslider.attr('data-target');
	var sel2 = sideslider.attr('data-target-2');
	sideslider.click(function(event){
		$(sel).toggleClass('in');			
		$(sel2).toggleClass('out');			
	});

    $('.home-blog .owl-carousel').owlCarousel({
        loop:true,
        responsive:{
            0:{
                items:1,
                nav:true,
                navText: ['<','>'],
                loop:true
            },
            768:{
                items:2,
                nav:true,
                navText: ['<','>'],
                loop:true
            },
            1000:{
                items:4,
                nav:true,
                navText: ['<','>'],
                loop:true
            }
        },
        autoplay:true,
        autoplayHoverPause:false,
        autoplayTimeout: 3000
    });

	if( screen.width < 992 ) {

        $('body > .wrapper2 .menu-wrap a').each(function(){
            var url1 = $(this).attr('url1');
            if (url1) {
                $(this).attr('href', '/' + url1);
            }
        });

        $('.post-relation .owl-carousel').owlCarousel({
            loop:true,
            responsive:{
                0:{
                    items:1,
                    nav:true,
                    navText: ['<','>'],
                    loop:true
                },
                768:{
                    items:2,
                    nav:true,
                    navText: ['<','>'],
                    loop:true
                }
            },
            autoplay:true,
            autoplayHoverPause:false,
            autoplayTimeout: 3000
        });

		$( '.menu-wrap' ).css( 'overflow', 'scroll' );
		var mobile_bt = $( '.mobile-menu-button' );				
		var root_class = $( 'i.fa', mobile_bt ).attr( 'root' );
		var active_class = $( 'i.fa', mobile_bt ).attr( 'active' );

		mobile_bt.on( 'click', function() {
			if( $( '.menu-wrap' ).hasClass( 'active' ) ) {
				$( '.menu-wrap' ).css( 'left', '-100%' ).removeClass( 'active' );
				$( 'i.fa', mobile_bt ).removeClass( active_class ).addClass( root_class );
            } else {
				$( '.menu-wrap' ).css( 'left', '0' ).addClass( 'active' );
				$( 'i.fa', mobile_bt ).removeClass( root_class ).addClass( active_class );
            }

            $('section').click(function () {
                mobile_bt.trigger('click');
            });

            $('div.content-left').click(function () {
                mobile_bt.trigger('click');
            });

            $('div.content-right').click(function () {
                mobile_bt.trigger('click');
            });

			if( $( 'body' ).hasClass( 'no-scroll' ) ) {
				$( 'body' ).removeClass( 'no-scroll' );
			} else {
				$( 'body' ).addClass( 'no-scroll' );
			}
		});

        $( '.header-mobile .dropdown b.fa-caret-down').click(function() {
            var rel = $(this).attr('rel');
            if (rel == 'active') {
                $(this).parent().find('.dropdown-menu').css('display','none');
                $(this).attr('rel','');
            } else {
                $(this).parent().find('.dropdown-menu').css('display','block');
                $(this).attr('rel','active');
            }
        });

        $( '.header-mobile .dropdown a[href="javascript:void(0)"]').click(function() {
            var rel = $(this).attr('rel');
            if (rel == 'active') {
                $(this).parent().find('.dropdown-menu').css('display','none');
                $(this).attr('rel','');
            } else {
                $(this).parent().find('.dropdown-menu').css('display','block');
                $(this).attr('rel','active');
            }
        });

		$( document ).on( 'swiperight', function() {
			$( '.menu-wrap' ).css( 'left', '0' ).addClass( 'active' );
			$( 'i.fa', mobile_bt ).removeClass( root_class ).addClass( active_class );
			$( 'body' ).addClass( 'no-scroll' );
		});
		
		$( document ).on( 'swipeleft', function() {
			$( '.menu-wrap' ).css( 'left', '-100%' ).removeClass( 'active' );
			$( '.menu-wrap' ).removeClass( 'active' ).slideToggle(250);
			$( 'i.fa', mobile_bt ).removeClass( active_class ).addClass( root_class );
			$( 'body' ).removeClass( 'no-scroll' );
		});
	}

    $('#contact_form').submit(function(event){
        var fullname_contact = null;
        var phone_contact = null;
        var email_contact = null;
        var subject_contact = null;
        var message_contact = null;
        var captcha = null;

        var form_data = $(this).serializeArray();
        form_data.forEach(function(data, index){
            if(data['name'] == 'fullname'){
                fullname_contact = data['value'];
            }
            if(data['name'] == 'phone'){
                phone_contact = data['value'];
            }
            if(data['name'] == 'email'){
                email_contact = data['value'];
            }
            if(data['name'] == 'subject'){
                subject_contact = data['value'];
            }
            if(data['name'] == 'message'){
                message_contact = data['value'];
            }
            if(data['name'] == 'g-recaptcha-response'){
                captcha = data['value'];
            }
        });

        if (!captcha) {
            alert('Vui lòng xác nhận bạn không phải là người máy');
            return false;
        }

        $.ajax({
            url: "/ajax/",
            type: 'POST',
            data: { add_contact: 1, fullname: fullname_contact, email: email_contact, phone: phone_contact, subject: subject_contact, message: message_contact, gcaptcha: captcha},
            beforeSend: function( xhr ) {
                $('#result_contact').html("<img src='/upload/loading.gif' style='display: inline-block;' /> Đang xử lý gửi thư liên hệ...");
            }
        }).done(function( data ) {
            if( data == 200 ){
                alert("Gửi thư thành công. Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất.");
                //set empty input value
                $('#fullname_contact').val("");
                $('#phone_contact').val("");
                $('#subject_contact').val("");
                $('#email_contact').val("");
                $('#message_contact').val("");
                $('#result_contact').html("");
            }else{
                alert(data);
                $('#result_contact').html("");
                return false;
            }
        });
        event.preventDefault();
    });

	/*** begin: gửi liên hệ ***/
	$('#popup-form').submit(function(event){
		var fullname_popup = null;
		var phone_popup = null;
		var email_popup = null;
		var message_popup = null;

        var form_data = $(this).serializeArray();
        form_data.forEach(function(data, index){
            if(data['name'] == 'fullname_popup'){
                fullname_popup = data['value'];
            }
            if(data['name'] == 'phone_popup'){
                phone_popup = data['value'];
            }
            if(data['name'] == 'email_popup'){
                email_popup = data['value'];
            }
            if(data['name'] == 'message_popup'){
                message_popup = data['value'];
            }
        });

        if (fullname_popup == '' || phone_popup == '' || email_popup == '') {
            alert('Dữ liệu không được để trống');
            return false;
        }

        $.ajax({
		  url: "/ajax/",
		  type: 'POST',
		  data: { add_contact_popup: 1, fullname: fullname_popup, email: email_popup, phone: phone_popup, message: message_popup },
		  beforeSend: function( xhr ) {
			$('.btn-popup').html("Đang xử lý...");
		  }
		}).done(function( data ) {
			if( data == 200 ){
				alert("Gửi thư thành công. Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất.");
				//set empty input value
				$('#fullname_popup').val("");
				$('#phone_popup').val("");
				$('#email_popup').val("");
				$('#message_popup').val("");
                $('.btn-popup').html("Gửi");
                setTimeout(function(){ $('#popup-modal').modal('hide'); }, 3000);
			}else{
				alert(data);
                $('.btn-popup').html("Gửi");
				return false;
			}
		});
		event.preventDefault();
	});

    $('#contact_form2').submit(function(event){
        var fullname_contact = null;
        var phone_contact = null;
        var email_contact = null;
        var subject_contact = null;
        var message_contact = null;
        var captcha = null;

        var form_data = $(this).serializeArray();
        form_data.forEach(function(data, index){
            if(data['name'] == 'fullname'){
                fullname_contact = data['value'];
            }
            if(data['name'] == 'phone'){
                phone_contact = data['value'];
            }
            if(data['name'] == 'email'){
                email_contact = data['value'];
            }
            if(data['name'] == 'subject'){
                subject_contact = data['value'];
            }
            if(data['name'] == 'message'){
                message_contact = data['value'];
            }
            if(data['name'] == 'g-recaptcha-response'){
                captcha = data['value'];
            }
        });

        if (!captcha) {
            alert('Vui lòng xác nhận bạn không phải là người máy');
            return false;
        }

        $.ajax({
            url: "/ajax/",
            type: 'POST',
            data: { add_contact: 1, fullname: fullname_contact, email: email_contact, phone: phone_contact, subject: subject_contact, message: message_contact, gcaptcha : captcha},
            beforeSend: function( xhr ) {
                $('#result_contact2').html("<img src='/upload/loading.gif' style='display: inline-block;' /> Đang xử lý gửi thư liên hệ...");
            }
        }).done(function( data ) {
            if( data == 200 ){
                alert("Gửi thư thành công. Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất.");
                //set empty input value
                $('#fullname_contact2').val("");
                $('#phone_contact2').val("");
                $('#subject_contact2').val("");
                $('#email_contact2').val("");
                $('#message_contact2').val("");
                $('#result_contact2').html("");
            }else{
                alert(data);
                $('#result_contact2').html("");
                return false;
            }
        });

        event.preventDefault();
    });
	/*** end: gửi liên hệ ***/

    $("a[rel^='prettyPhoto']").prettyPhoto({
        social_tools:false,
        deeplinking: false
    });

    $('.sidebar-category a.menu').click(function(){
        $(this).toggleClass("menu-open");
    });

    $('.home-product .menu ul li').click(function(){
        var index = $(this).attr('rel');
        $('.home-product-content').hide();
        $('.home-product-content' + index).fadeIn();
        $('.home-product .content').hide();
        $('.home-product .content' + index).fadeIn();
        if ($('.home-product .menu li').hasClass('active')) {
            $('.home-product .menu li').removeClass('active');
        }
        $(this).addClass('active');
    });

    $('.big-video-youtube i').click(function(){
        var rel = $(this).attr('rel');
        showVideo(rel, '.big-video-youtube', 500);
    });
    $('.home-video-relation p').click(function(){
        var rel = $(this).attr('rel');
        showVideo(rel, '.big-video-youtube', 500);

        $('.home-video-relation').find('.active-video').removeClass('active-video');
        $(this).addClass('active-video')

    });
    $('.home-video-relation img').click(function(){
        var rel = $(this).attr('rel_id');
        showVideo(rel, '.big-video-youtube', 500);

        $('.home-video-relation').find('.active-video').removeClass('active-video');
        $(this).parent().find('h5').addClass('active-video')
    });
    $('.youtube-channel i').click(function(){
        var rel = $(this).attr('rel');
        showVideo(rel, '.youtube-channel', 250);
    });

    var rating_id = $("#rating_count");
    var rating_count = rating_id.attr('count');
    $('#rating').starrr({
        rating: rating_count,
        change: function(e, value){
            if (value) {
                var rating_id = $("#rating");
                var rel = rating_id.attr('rel');
                var path = rating_id.attr('path');
                $.ajax({
                    url: "/ajax/",
                    type: 'POST',
                    data: {rating: 1, count: value, loc: rel, path: path}
                }).done(function( data ) {
                    if( data == 200 ){
                        alert("Cảm ơn bạn đã đánh giá bài viết với chất lượng " + value + " sao");
                        return false;
                    }else{
                        alert(data);
                        return false;
                    }
                });
                event.preventDefault();
            }
        }
    });

    var enable_jssor = $('#jssor_1').attr('rel');
    if (enable_jssor == 'enable') {
        jssor_1_slider_init();
    }

    //change view
    $('#list-view').click(function(){
        $(this).css('color', '#5A9D04');
        $('#grid-view').css('color', '#000');
        $('.post-list-article').addClass('post-list-article-list');
        var item_article = $('.post-list-article .item-article');
        item_article.removeClass('col-md-4').addClass('col-md-12');
        $('.post-list-article .item-article > a').addClass('col-md-3').addClass('padding8px');
        $('.post-list-article .item-article h4.title').css('min-height', 0);
    });

    $('#grid-view').click(function(){
        $(this).css('color', '#5A9D04');
        $('#list-view').css('color', '#000');
        $('.post-list-article').removeClass('post-list-article-list');
        var item_article = $('.post-list-article .item-article');
        item_article.removeClass('col-md-12').addClass('col-md-4');
        $('.post-list-article .item-article > a').removeClass('col-md-3').removeClass('padding8px');
        $('.post-list-article .item-article h4.title').css('min-height', '45px');
    });
})

function showVideo(id, selector, height) {
    $(selector).html('<iframe width="100%" height="'+ height +'" src="https://www.youtube.com/embed/'+ id +'?rel=0&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
}

function loadFacebookAPI(app_id) {
    var js = document.createElement('script');
    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=' + app_id + '&autoLogAppEvents=1';
    js.setAttribute('async',"");
    js.setAttribute('defer',"");
    js.setAttribute('crossorigin',"anonymous");
    document.body.appendChild(js);
}

function loadRecaptchaAPI(path) {
    var js = document.createElement('script');
    js.src = path;
    js.setAttribute('async',"");
    js.setAttribute('defer',"");
    document.body.appendChild(js);
}

function googleTranslateElementInit2() {
    new google.translate.TranslateElement({
        pageLanguage: 'vi',
        autoDisplay: false
    }, 'google_translate_element2');
}



window.onscroll = function () {
    var recapt_contact1 = document.getElementById('recapt_contact1');
    if (recapt_contact1) {
        var rect = document.getElementById('recapt_contact1').getBoundingClientRect();
        var path_recapt = document.getElementById('recapt_contact1').getAttribute('data-id');
    }
    var face_app_id = document.getElementById('face-pages');
    if (!rect && face_app_id) {
        rect = document.getElementById('face-pages').getBoundingClientRect();
	}

    if (face_app_id) {
        var face_app_id = document.getElementById('face-pages').getAttribute('data-app-id');
    }

    if (rect && rect.top < window.innerHeight) {
        if (recapt_contact1) {
            loadRecaptchaAPI(path_recapt);
        }
        loadFacebookAPI(face_app_id);
        window.onscroll = null;
    }
}

/*** begin: fb plugins ***/
var fb_enable = $('.post-comment').attr("fb-enable");
var fb_rel = $('.post-comment').attr("fb-rel");
if( fb_enable == 1 && fb_rel){
	window.fbAsyncInit = function() {
		FB.init({
			appId      : fb_rel,
			xfbml      : true,
			version    : 'v2.8'
		});
	};

	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
}
/*** end: fb plugins ***/

$(window).load(function () {
    var js = document.createElement('script');
    js.src = "https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2";
    document.body.appendChild(js);

    var sharer_enable = $('.sharer').attr("sharer-enable");
    if (sharer_enable == 1) {
        var js_sharer = document.createElement('script');
        js_sharer.src = "//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5cb407b5a2b7a2b4";
        document.body.appendChild(js_sharer);
    }

    var enable_swiper = $('.swiper-container').attr('rel');
    if (enable_swiper) {
        var swiper = new Swiper('.swiper-container', {
            autoplay: {
                delay: 4000
            },
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows : true
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            }
        });
    }
});

/* <![CDATA[ */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('6 7(a,b){n{4(2.9){3 c=2.9("o");c.p(b,f,f);a.q(c)}g{3 c=2.r();a.s(\'t\'+b,c)}}u(e){}}6 h(a){4(a.8)a=a.8;4(a==\'\')v;3 b=a.w(\'|\')[1];3 c;3 d=2.x(\'y\');z(3 i=0;i<d.5;i++)4(d[i].A==\'B-C-D\')c=d[i];4(2.j(\'k\')==E||2.j(\'k\').l.5==0||c.5==0||c.l.5==0){F(6(){h(a)},G)}g{c.8=b;7(c,\'m\');7(c,\'m\')}}',43,43,'||document|var|if|length|function|GTranslateFireEvent|value|createEvent||||||true|else|doGTranslate||getElementById|google_translate_element2|innerHTML|change|try|HTMLEvents|initEvent|dispatchEvent|createEventObject|fireEvent|on|catch|return|split|getElementsByTagName|select|for|className|goog|te|combo|null|setTimeout|500'.split('|'),0,{}))
/* ]]> */

/*** jssor ***/
jssor_1_slider_init = function() {

    var jssor_1_SlideshowTransitions = [
        {$Duration:1200,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,x:-0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,y:0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,y:-0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,y:-0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,y:0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,x:0.3,$Cols:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,x:0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,y:0.3,$Rows:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,y:0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,y:0.3,$Cols:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,y:-0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,x:0.3,$Rows:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,x:-0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,$Delay:20,$Clip:3,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,$Delay:20,$Clip:3,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,$Delay:20,$Clip:12,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
        {$Duration:1200,$Delay:20,$Clip:12,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
    ];

    var jssor_1_options = {
        $AutoPlay: 1,
        $SlideshowOptions: {
            $Class: $JssorSlideshowRunner$,
            $Transitions: jssor_1_SlideshowTransitions,
            $TransitionsOrder: 1
        },
        $ArrowNavigatorOptions: {
            $Class: $JssorArrowNavigator$
        },
        $ThumbnailNavigatorOptions: {
            $Class: $JssorThumbnailNavigator$,
            $SpacingX: 5,
            $SpacingY: 5
        }
    };

    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

    /*#region responsive code begin*/

    var MAX_WIDTH = 980;

    function ScaleSlider() {
        var containerElement = jssor_1_slider.$Elmt.parentNode;
        var containerWidth = containerElement.clientWidth;

        if (containerWidth) {
            var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);
            if (screen.width < 992) {
                expectedWidth += 200;
            }
            //console.log(containerWidth + '==' + expectedWidth);
            jssor_1_slider.$ScaleWidth(expectedWidth);
        }
        else {
            window.setTimeout(ScaleSlider, 30);
        }
    }

    ScaleSlider();

    $Jssor$.$AddEvent(window, "load", ScaleSlider);
    $Jssor$.$AddEvent(window, "resize", ScaleSlider);
    $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
    /*#endregion responsive code end*/
};