<?php if(!defined('_lib')) die("Error");

	function get_score($price){
			global $d, $row_score;
			$sql = "select score from table_user_score where hienthi=1 order by gia desc";
			$d->query($sql);
			$row_score = $d->result_array();
			for($i=0;$i<count($row_score);$i++)
			{
				if($price>=$row_score[$i]['gia'])
				{
					return $row_score[$i]['score'];
					break;
				}
			}


			
		}

	function get_code($pid){
			global $d, $row;
			$sql = "select masp from table_product where id=$pid";
			$d->query($sql);
			$row = $d->fetch_array();
			return $row['masp'];
		}
		
	function get_product_name($pid){
		global $d, $row;
		$sql = "select ten from #_product where id=$pid";
		$d->query($sql);
		$row = $d->fetch_array();
		return $row['ten'];
	}
	function get_tong_tien($id=0){
		global $d;
		if($id>0){
			$d->reset();
			$sql="select gia,soluong from #_chitietdonhang where madonhang='".$id."'";
			$d->query($sql);
			$result=$d->result_array();
			$tongtien=0;
			for($i=0,$count=count($result);$i<$count;$i++) { 
			
			$tongtien+=	$result[$i]['gia']*$result[$i]['soluong'];	
			
			}
			return $tongtien;
		}else return 0;
	}
	function get_product_photo($pid){
		global $d, $row;
		$sql = "select thumb from #_product where id=$pid";
		$d->query($sql);
		$row = $d->fetch_array();
		return $row['thumb'];
	}
	
	function get_price($pid,$id_price){
		global $d, $row;

		if($id_price>0)
		{
			$sql = "select price from table_product_price where id=$id_price";
			$d->query($sql);
			$row = $d->fetch_array();
			return (int)preg_replace('/[^0-9]/','',$row['price']);
		}
		else
		{
			$sql = "select gia from table_product where id=$pid";
			$d->query($sql);
			$row = $d->fetch_array();
			return (int)preg_replace('/[^0-9]/','',$row['gia']);
		}

		
	}
	
	function remove_product($pid,$size,$id_price){
		$pid=intval($pid);
		$max=count($_SESSION['cart']);
		for($i=0;$i<$max;$i++){
			if($pid==$_SESSION['cart'][$i]['productid'] and $id_price==$_SESSION['cart'][$i]['id_price']){
				unset($_SESSION['cart'][$i]);
				break;
			}
		}
		$_SESSION['cart']=array_values($_SESSION['cart']);
	}
	
	function get_order_total(){
		$max=count($_SESSION['cart']);
		$sum=0;
		for($i=0;$i<$max;$i++){
			$pid=$_SESSION['cart'][$i]['productid'];
			$q=$_SESSION['cart'][$i]['qty'];
			$id_price = $_SESSION['cart'][$i]['id_price'];
			$price=get_price($pid,$id_price);
			$sum+=$price*$q;
		}
		return $sum;
	}
	
	function get_total(){
		$max=count($_SESSION['cart']);
		$sum=0;
		for($i=0;$i<$max;$i++){
			$pid=$_SESSION['cart'][$i]['productid'];
			$q=$_SESSION['cart'][$i]['qty'];
			$sum+=$q;
		}
		return $sum;
	}
		
	function addtocart($pid,$size,$id_price,$q){
		if($pid<1 or $q<1) return;
		
		if(is_array($_SESSION['cart'])){
			if(product_exists($pid,$size,$id_price)) return;
			$max=count($_SESSION['cart']);
			$_SESSION['cart'][$max]['productid']=$pid;
			$_SESSION['cart'][$max]['qty']=$q;
			$_SESSION['cart'][$max]['size']=$size;
			$_SESSION['cart'][$max]['id_price']=$id_price;
		}
		else{
			$_SESSION['cart']=array();
			$_SESSION['cart'][0]['productid']=$pid;
			$_SESSION['cart'][0]['qty']=$q;
			$_SESSION['cart'][0]['size']=$size;
			$_SESSION['cart'][0]['id_price']=$id_price;
		}
	}
	
	function product_exists($pid,$size,$id_price){
		$pid=intval($pid);
		$max=count($_SESSION['cart']);
		$flag=0;
		for($i=0;$i<$max;$i++){
			if($pid==$_SESSION['cart'][$i]['productid'] and $size==$_SESSION['cart'][$i]['size'] and $id_price==$_SESSION['cart'][$i]['id_price']){
				$flag=1;
				break;
			}
		}
		return $flag;
	}

?>